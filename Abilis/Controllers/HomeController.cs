﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Abilis.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int size = 10, Models.ShowType showType = Models.ShowType.Decimal)
        {
            if (size < 3)
                size = 3;
            else if (size > 15)
                size = 15;

            return View(new Models.MultiplicationTableModel() {
                size = size + 1,
                showType = showType
            });
        }
    }
}