﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Abilis.Models
{
    public class MultiplicationTableModel
    {
        public int size { get; set; }
        public ShowType showType { get; set; }
    }

    public enum ShowType
    {
        Decimal = 0,
        Hex = 1,
        Binary = 2
    }
}