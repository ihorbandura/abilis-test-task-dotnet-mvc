﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Abilis.Helpers
{
    public class WebHelper
    {
        public static string GetConvertedNumByShowType(int num, Models.ShowType showType)
        {
            if (showType == Abilis.Models.ShowType.Decimal)
                return Convert.ToString(num);
            else if (showType == Abilis.Models.ShowType.Hex)
                return Convert.ToString(num, 16);
            else
                return Convert.ToString(num, 2);
        }
    }
}