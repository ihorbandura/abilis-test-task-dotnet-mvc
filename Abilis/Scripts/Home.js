﻿function hover_unhover_elm(elm, ishovered) {
    var hovered_classname = "hovered-cell";

    if (ishovered)
        elm.addClass(hovered_classname);
    else
        elm.removeClass(hovered_classname);

    var hovered_col_id = elm.data("col-id");
    var hovered_row_id = elm.data("row-id");

    var hovered_col = $('th[data-col-id=' + hovered_col_id + ']');
    var hovered_row = $('th[data-row-id=' + hovered_row_id + ']');

    if (ishovered) {
        hovered_col.addClass(hovered_classname);
        hovered_row.addClass(hovered_classname);
    } else {
        hovered_col.removeClass(hovered_classname);
        hovered_row.removeClass(hovered_classname);
    }
}

function addNewURLParameter(url, parameterName, parameter) {
    if (url.indexOf('?') < 0)
        url += "?";
    else
        url += "&";

    return url + parameterName + "=" + parameter;
}

function submitButtonClick(actionUrl) {
    var matrix_size = $('#matrix-size').val();
    var matrix_base = $('#matrix-base').val();

    if (matrix_size != null && matrix_size.length > 0)
        actionUrl = addNewURLParameter(actionUrl, "size", matrix_size);

    if (matrix_base != null && matrix_base.length > 0)
        actionUrl = addNewURLParameter(actionUrl, "showType", matrix_base);

    location.href = actionUrl;
}

$(document).ready(function () {
    $('.js-hover-cell').hover(function () {
        var hovered_elm = $(this);
        hover_unhover_elm(hovered_elm, true);
    },
    function () {
        var unhovered_elm = $(this);
        hover_unhover_elm(unhovered_elm, false);
    });
});